// 確保 Gitlab項目的名稱替換了 'test-project'
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: process.env.NODE_ENV === 'production' ? '/test-project/' : '/',
})
