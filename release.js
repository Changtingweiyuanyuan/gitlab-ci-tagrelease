const fs = require("fs");
const request = require("request");

const apiUrl = "https://gitlab.com/api/v4";
const projectId = process.env.projectId;
const accessToken = process.env.accessToken;

const tagName = process.env.CI_COMMIT_TAG;
const releaseName = `Release ${tagName}`;
const releaseDescription = `This is the ${tagName} release.`;

const asset = { filepath: "release.zip", filename: "release.zip" };
const assetBuffer = {
  buffer: fs.readFileSync(asset.filepath),
  name: asset.filename,
};

request.post(
  `${apiUrl}/projects/${projectId}/releases`,
  {
    headers: { "PRIVATE-TOKEN": accessToken },
    json: {
      name: releaseName,
      description: releaseDescription,
      assets: [assetBuffer],
    },
  },
  (error, response, body) => {
    if (error) {
      console.error(error);
    } else {
      console.log(body);
    }
  }
);